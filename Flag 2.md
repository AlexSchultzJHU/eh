## Flag Info:  

Variable | Value
------------ | -------------
Flag No. | 2
Flag Type | Medium
Flag ID | 2
System |  Sales
OS |  Windows XP
Est TTC | 25
UserName | dschru
PassWord | password
Static IP | 192.168.20.5

## Flag Requirements:  
- [ ] Gain Dwights Sales account access.

## Flag Intro:
After gaining access through privilege escalation and obtaining Dwight's password, Michael is able to access Dwight's account on the Sales server. He knows Dwight likes to play games, so he doesn't expect to find the copy of the complaint he filed to just be lying around in plain sight. Michael starts to search through the files on the account, but can't seem to find any regarding a complaint. He remembers that the brief said that the complaint should be in the Work folder on the Desktop, but he can't seem to find it. He then remembers that the brief said "Not everything is in plain sight." He suspects the file could be hidden, so he uses his "hacking" skills to look for hidden files on the account. He types "dir /a:h" into the command prompt and finds a folder on Dwight's desktop named Work, but there only seems to be one picture in it that has nothing to do with what he is looking for and is not named "complaint"

## Flag Walkthrough:
1. Find the hidden Work folder
2. Find the hidden complaint file
3. Change file attributes to be visible
4. Determine actual file type ( .docx )
5. Change file extension 
6. Read .docx file for next flag


## Flag Sequance:  

```mermaid
stateDiagram
 state cmdLine 
  [*] --> DesktopFolder : cd Desktop
  note right of  DesktopFolder
      The Work folder will not appear with basic dir, must use dir /a<colon>h
  end note
  DesktopFolder --> WorkFolder : cd Work
  note right of  WorkFolder
      The complaint file will be hidden, must use dir /a<colon>h
  end note
  WorkFolder --> DiscoverMagicNumber : Requires finding file type, .docx is 504B0304
  WorkFolder --> Contents : attrib -h -s complaint.png
  note right of Contents
      Change visability
  end note
  Contents --> MoveContents: move complaint.png complaint.docx
  note right of MoveContents
      Execute Word to View file
  end note
  MoveContents --> ViewContents : "C<colon>Program Files |Microsoft Office\Office12\WINWORD.exe" complaint.docx

```

## Flag Notes:  
Although the brief said that the complaint should be in the Work folder, "Not everything is in plain sight" was repeated twice. He then suspects that the file is hidden too. He opens up a command prompt and checks for hidden files in this folder by executing the command "dir /a:h" AHA! He was right! There seems to be a hidden file named "complaint" but confusingly it has a .png extension.

