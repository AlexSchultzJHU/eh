## Flag Info:  

Variable | Value
------------ | -------------
Flag No. | 7
Flag Type | Hard
Flag ID | 7
System |  Sales
OS |  Windows XP
Est TTC | 45
UserName | dschru
PassWord | password
Static IP | 192.168.20.5

## Flag Requirements:  
- [ ] Knowledge of Windows Scripting

## Flag Intro:
Michael wants to run up the computing resources of the Sales team in order to limit their ability to access their necessary documents, programs, and processing of sales discreetly. In order to do so, he will create a VBscript that launches 5  programs: vlc.exe, AcroRd32.exe, HxD.exe, iexplore.exe, moviemk.exe in invisible mode.  

## Flag Walkthrough:
1. Michael wants to run up the computing resources of the Sales team in order to limit their ability to access their necessary documents, programs, and processing of sales discreetly. In order to do so, he will create a VBscript that launches 5  programs: vlc.exe, AcroRd32.exe, HxD.exe, iexplore.exe, moviemk.exe in invisible mode.  

2. His first few steps are to create a new text file and save it as a .vbs This creates a visual basic script which will allow you to manipulate programs 

3. Image download failed.
  

4. Using this site: https://winaero.com/blog/run-a-program-hidden-in-windows-10/, develop the appropriate script to launch the aforementioned applications in invisible mode 

``` Example: referanced ```

5. Once created, the final step is to actually run the script. If the script was successfully created and run, within 5 seconds, a message should appear. 



```

## Flag Notes:  
See image from Ios
