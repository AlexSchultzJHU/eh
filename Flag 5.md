## Flag Info:  

Variable | Value
------------ | -------------
Flag No. | 4
Flag Type | Medium
Flag ID | 4
System |  H.R.
OS |  Ubuntu 16 or 18
Est TTC | 20
UserName | HR System
PassWord | EthicalCTF2020
Static IP | 192.168.20.20

## Flag Requirements:  
- [ ] HRs .ova running, may need to be told password

## Flag Intro:
Find the complaints on the HR system

## Flag Walkthrough:
1. Find png fiel on desktop
2. Look inside, simple image
3. Terminal: cd Desktop
4. file *.png -> it is a png file!
5. Attempt to unzip
6. Results in secret folder
7. Inside we find txt and image files
8. read hint and use tool steghide
9. steghide extract -sf new_log.jpg
10. enter supplied password
11. Result in new txt, read and navigate to complaint folder
12. Read complaints, result in hint

