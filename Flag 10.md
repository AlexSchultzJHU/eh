## Flag Info:  

Variable | Value
------------ | -------------
Flag No. | 10
Flag Type | Easy
Flag ID | 10
System |  Pam
OS |  Raspbian
Est TTC | 10
UserName | 
PassWord | 
Static IP | 192.168.1.*

## Flag Requirements:  
- [ ] Network Configuration
- [ ] Internet Access: Github
- [ ] Other VMs up and running

## Flag Intro:
Michael is now ready to take over, so his last move is to shutdown all of the other machines on the network. Once they are down then it is up. 


## Flag Walkthrough:
1. Our enemy team should get the flag 9 before they go for the flag 10. They will get a download linkand a hints message --- "Raspbian?flagten" from flag 9.
2. 2. “flagten” is the unzip password of the file they download from my link. But they will not know it isa 7z zip file until they make some analyzations on it.
3. 3. once  they  unzip  the  file,  they  will  get  an  executable  file.  This  executable  file  can  be  only  run  inRaspbian. They also do not know that until they make some analyzations on it.
4. 4. They should give the executable permission to this file. Once they run this executable file correctlyon Raspbian, they should follow the instructions which are showed in the program. And once theyturn on all the other three VMs and then turn them off, they can reach to the flag --- the console willprint out a message says “Congrats! You got flag 10! You are the winner!!”.
 

```

