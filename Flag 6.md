## Flag Info:  

Variable | Value
------------ | -------------
Flag No. | 6
Flag Type | 
Flag ID | 6
System |  Sales
OS |  Windows XP
Est TTC | 20
UserName | dschru
PassWord | password
Static IP | 192.168.20.5

## Flag Requirements:  
- [ ] Prev flags completion

## Flag Intro:
In order to read the file, we will need to find a password for the Price List file on Dwights Sales account.

## Flag Walkthrough:
1. Find the encrypted file, Price List.pdf on the sales machine. The file is password-protected.
2. Find the decryption software, APDFPR, and run it. The following steps must be followed in order to decrypt efficiently. 
3. Select "All digits" option only on Range setting; do not select anything else to speed up the process.
4. Set the length to be 0 to 6; with the default setting, decryption cannot be performed. 
5. Open the file and click "Start recovery"
6. If setting is correct, then the process should take less than 1 minute, and the correct password "851100" should appear



## Flag Notes:  
All required decryption software is on the computer