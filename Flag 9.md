## Flag Info:  

Variable | Value
------------ | -------------
Flag No. | 9
Flag Type | Medium
Flag ID | 9
System |  N/A ( users )
OS |  N/A ( users )
Est TTC | N/A ( users )
UserName | N/A ( users )
PassWord | N/A ( users )
Static IP | N/A ( users )

## Flag Requirements:  
- [ ] Programming experiance 

## Flag Intro:
We've found a cyper script, but we can't figure it out by hand. Thus we'll need to break it to find a link for Flag 10.

## Flag Walkthrough:
1. find the hidden directory path in the source code of the web page.

2. find the hidden fomula in the source code of the web page and use base64 to decode.

3. find two strang numbers in the web page 23 and 36 as k1 and k2 for the formula.

4. Write a small program to decrypt the ciphertext with the formula and k1 k2.

5. get the flag.


```

## Flag Notes:  
k1 = 23
k2 = 36
plaintext = ""
​
ciphertext = "Kdd xywi mi vkdiy. Fpy euarkxq ciyi mxvylmul akfylmkdi kxb iryemkddq iydyefi suub rlubcefi vul fyifmxs. Mx vkef, fpy euarkxq iyelyfdq ecf fpy ryximux hyxyvmfi uv mfi yarduqyyi. Fpy euarkxq vuleyi yzylq yarduqyy fu lkmiy auxyq kxb fpyx buxkfy fpy auxyq fu ruul yarduqyyi mx fpy xkay uv fpy euarkxq. Fpy euarkxq iyelyfdq bmiepklsyb kdd fpy rkryl iywksy mxfu fpy xyklhq lmzyl.      Fpy pmxf uv vmxkd vdks: pffri://smfpch.eua/EpyxPckxsqmx/Educb_vmdy/lkw/akifyl/vdks10  gyq wulbi: lkirhmkx, vdksfyx"
​
for element in ciphertext:
    
    if element.isalpha():
        if element.islower():
            c = ord(element) - ord('a')
            for x in range(26):
                if c == (x * k1 + k2) % 26:
                    plaintext = plaintext + chr(x+ord('a'))
                    break
        
        else:
            c = ord(element) - ord('A')
            for x in range(26):
                if c == (x * k1 + k2) % 26:
                    plaintext = plaintext + chr(x+ord('A'))
                    break 
        
    else:
        plaintext = plaintext + element
    
print(plaintext)

will lead to https://github.com/ChenHuangyin/Cloud_file/raw/master/flag10