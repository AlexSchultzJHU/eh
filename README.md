```mermaid

graph TD

FlagOne -- Sales --> FlagTwo
FlagTwo -- Sales --> FlagThree
FlagThree -- Accounting --> FlagFour
FlagFour -- Pam --> FlagFive
FlagFive -- Sales --> FlagSix
FlagSix -- Sales --> FlagSeven
FlagSeven -- Sales --> FlagEight
FlagEight -- Users --> FlagNine
FlagNine -- Pam --> FlagTen

click FlagOne "https://gitlab.com/AlexSchultzJHU/eh/-/blob/master/Flag%201.md"
click FlagTwo "https://gitlab.com/AlexSchultzJHU/eh/-/blob/master/Flag%202.md"
click FlagThree "https://gitlab.com/AlexSchultzJHU/eh/-/blob/master/Flag%203.md"
click FlagFour "https://gitlab.com/AlexSchultzJHU/eh/-/blob/master/Flag%204.md"
click FlagFive "https://gitlab.com/AlexSchultzJHU/eh/-/blob/master/Flag%205.md"
click FlagSix "https://gitlab.com/AlexSchultzJHU/eh/-/blob/master/Flag%206.md"
click FlagSeven "https://gitlab.com/AlexSchultzJHU/eh/-/blob/master/Flag%207.md"
click FlagEight "https://gitlab.com/AlexSchultzJHU/eh/-/blob/master/Flag%208.md"
click FlagNine "https://gitlab.com/AlexSchultzJHU/eh/-/blob/master/Flag%209.md"
click FlagTen "https://gitlab.com/AlexSchultzJHU/eh/-/blob/master/Flag%2010.md"


```