## Flag Info:  

Variable | Value
------------ | -------------
Flag No. | 1
Flag Type | Easy
Flag ID | Start
System |  Sales
OS |  Windows XP
Est TTC | 15 min
UserName | None 
PassWord | None
Static IP | 192.168.20.5

## Flag Requirements:  
- [ ] Start VMs

## Flag Intro:
 
At the founding of Dunder Mifflin's new computer system, Michael Scott knows everybody's account was set up with the first letter of their first name and first 5 letters of their last name. Further, their password initially matched their usernames. Competent employees will have changed their password, while interns made theirs blank. For example Michael Scotts username:password was mscott:mscott. 

 
Michael suspects that Dwight Schrute submitted the complaint that got him fired.  

## Flag Walkthrough:
1. Access pvances computer
2. Read Gossip Files
3. Determine the Admin (dcroda) password is 31415926
4. Enter drodas System, attempt to access Dwights: Access Denied
5. Attempt to access jhalpe find SuperSecretFolder
6. Find Dwights passwword
7. Access Dights computer
8. Hearing The Office Theme denotes suceess
 

## Flag Sequance:  

```mermaid
sequenceDiagram

Vance->>Halpert: Privilege Escalation to access Shared Folder
Halpert->>Schrute: Halpert Has Dwights password for pranks.

```
or



## Flag Notes:  
All accounts are on the same Windows XP computer.  

User groups can have access to shared folders. The objective is privilege escalate one user to that group so they can read the next flag.  Alternatively Clues have been left to allow for a social engineering attack, should attackers choose.   

My flag will be a document in a shared folder that has Dwight's account's password.   

As Flag 1 is the easiest Dwight's password has been set to password. Alternatively, to make it more complicated such that they can brute force it. Or give a clue that a certain users password is empty.   
  
This flag is designated as 'easy' as there are multiple tools that can get to the next flag. 



