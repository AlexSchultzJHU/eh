## Flag Info:  

Variable | Value
------------ | -------------
Flag No. | 8
Flag Type | Medium - Hard
Flag ID | 8
System |  Acounting
OS |  Windows XP
Est TTC | 45
UserName | amart
PassWord | <none>
Static IP |  192.168.20.15

## Flag Requirements:  
- [ ] Previous flags meet all requiremetns

## Flag Intro:
Michael must find Dwights banking information on the Accounting Computer

## Flag Walkthrough:
PART 1: FIND THE FILE

1.1 Michael opens Accounting Windows XP VM and enters as user "amart" (no password).

Note: The method below uses the File Explorer, but they may solve it another way.

1.2 Michael opens the File Explorer and doesn't see any particularly interesting files.

1.3 He gets a hunch that the file might be hidden. He knows or looks up how to uncover hidden files on Windows XP.

One approach is to navigate to Control Panel > Switch to Classic View > Folder Options > View and then select “Show hidden files and folders” under the advanced settings.

This reveals the “BEETS” folder under My Documents.

1.4 The BEETS folder contains a bunch of files that seem to be vCard Files with random titles, where could the password be?

There is one TXT file, let’s see what it is.

It contains Dwight’s password!

File Contents:

“Dwight’s bank pass: FEBUW45DFOJXGK5BAMJ2XIIDIMUQGQ2LEEB2GQZJANRUW42ZAONXW2ZLXNBSXEZJAOVZWS3THHIQEU6JVGFLTQ6DZMR3VEVLSG4======I5XW6ZBBEBCHO2LHNB2CA4DVOQQHI2DFEBZGK43UEBXWMIDINFZSAYLDMNXXK3TUEBSGK5DBNFWHGIDTN5WWK53IMVZGKIDPNYQHI2D”

PART 2: DECODING THE PASSWORD

2.1 Michael has a hunch the password might be encoded to hide the rest of the information. He thinks it might be Base32 or Base 64 because of the equal signs but it looks like it is out of order.

Michael re-orders the password so that the equal signs are at the end:

FEBUW45DFOJXGK5BAMJ2XIIDIMUQGQ2LEEB2GQZJANRUW42ZAONXW2ZLXNBSXEZJAOVZWS3THHIQEU6JVGFLTQ6DZMR3VEVLSG4======I5XW6ZBBEBCHO2LHNB2CA4DVOQQHI2DFEBZGK43UEBXWMIDINFZSAYLDMNXXK3TUEBSGK5DBNFWHGIDTN5WWK53IMVZGKIDPNYQHI2D

I5XW6ZBBEBCHO2LHNB2CA4DVOQQHI2DFEBZGK43UEBXWMIDINFZSAYLDMNXXK3TUEBSGK5DBNFWHGIDTN5WWK53IMVZGKIDPNYQHI2DFEBUW45DFOJXGK5BAMJ2XIIDIMUQGQ2LEEB2GQZJANRUW42ZAONXW2ZLXNBSXEZJAOVZWS3THHIQEU6JVGFLTQ6DZMR3VEVLSG4======

2.2 He is now able to decode it and it is Base32! Recommended online decoder cryptii.com.

The decrypted password reveals a new step to the challenge to uncover the rest of his account information.

“Good! Dwight put the rest of his account details somewhere on the internet but he hid the link somewhere using: Jy51W8xydwRUr7”

2.3 The random string Dwight used looks just like the weird file names on all those vCard Files Michael found. Michael finds the VCF file with the matching filename in the same folder by looking or using a search tool.

2.4 How can he read this file?

Note: Because Windows Address Book is not set as our “default vCard reader”, our system doesn’t know how to open vCard Files easily right now. It will try to set up Outlook instead of just showing you the contents of the file or opening the file in Address Book. This means you’ve got to work around it a little!

Three Methods to reading the VCF file:

1. Rename the file to .txt and open it as normal.

2. Right-click on it and use the Open with… > Notepad option

3. Look up what a vCard File is, use Windows Address Book

a. It is a contact entry for the Windows Address Book

b. Open Start > All Programs > Accessories > Address Book

c. File > Import > Business Card and then select the matching file.

d. Read the Contact in the Address Book

PART 3: RETRIEVE THE FINAL MESSAGE

Once Michael has opened the VCF file, he sees a link to a pastebin paste under his work/business URL. Michael follows the URL (probably copying and pasting it outside of the VM to use the internet) and has successfully reached the end of Flag 8!

“https://pastebin.com/KnDCjRyT”



## Flag Notes:  
"Dwight messed with Michael’s future financial stability, now it’s time to return the favor. He wants to get Dwight's banking information so that he can steal his next paycheck. Dwight knew Michael would try to steal from him so he hid his bank account password in a file on Angela's profile (amart) on the Accounting system. Once you find the file, the password might be a little hard to understand. This may require you to switch systems.”
