## Flag Info:  

Variable | Value
------------ | -------------
Flag No. | 4
Flag Type | Medium
Flag ID | 4
System |  Reception
OS |  Rasbian
Est TTC | 30
UserName | pi
PassWord | Team1
Static IP | 192.168.20.10

## Flag Requirements:  
- [ ] External Ubunut/Kali box -> able to run nmap on Pams computer
- [ ] Pams comuter must be up and running

## Flag Intro:
List of Customers & Voicemail will lead to hint for flag 5. Requires Visiting an open webpage and identifying its open port

## Flag Walkthrough:
1. Scan for open ports on the machine: open port is 6532 web server 

2. Open web browser to 192.168.20.10:6532

3. Read voicemails to determine what the flag could be 

4. Use remote code execution through webserver to view file or exfiltrate the data ( ; cat /home/pam/Documents/customers.csv) 

5. Searcg for 'Flag' or go to last data entry
 



## Flag Notes:  
Story Hint: Acquires a list of customers and then recognizes that one of the numbers is on an unopened voicemail. The voicemail is a complaint against the company – Michael wants to steal this customer away. 

Usernames: 

u: root  p:ethicalhacking 

u: pi      p:Team1 

u: pam p:cantguess 

  