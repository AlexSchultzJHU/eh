## Flag Info:  

Variable | Value
------------ | -------------
Flag No. | 3
Flag Type | Medium
Flag ID | 3
System |  Accounting
OS |  Windows XP
Est TTC | 30m
UserName | amart
PassWord | <none>
Static IP | 192.168.20.15

## Flag Requirements:  
- [ ] Complete Flag 2

## Flag Intro:
Doles Ipsum

Flag Walkthrough:
1. A .jpg picture file of a computer is on the desktop of user "amart" 

2. File name: thefilesareinthecomputer.jpg ... (ha ha).. the file is the combination of a computer jpg and a zip file.  

3. If you open with a hex editor you will see the extra data at the end (more of it is in plaintext than I thought would be but that's ok) 

 
4. You find the end of the .jpg (hex: FF D9 - note there are a couple of these in there, but one is more clearly the end) and then cut out the rest of the file and save as a .zip (which they might need a hint on? since we aren't on linux).  

5. Once you open the zip file you will see a text file with an encoded hint (and a plaintext helpful message) and a salary.xls file. 

6. Text file provided to the user (hint.txt) 

7. ```VmggVmggcXhmIGNxbiBSWSBjamt1bmIgY2Rhdy4uLiBIeGQgamFuIGp1dnhiYyBjcW5hbi4gQ3FuIGJ4dWRjcnh3IHJiIHFybW1udyBydyBjcW4gYmp1amFoIHJ3b3hhdmpjcnh3IGJ4dm5mcW5hbiByd2VyYnJrdW4uIExqdyBoeGQgb3J3bSByYyB4dyB1cnduIHdyd253cnduPw== ```

8. what is this code? Hmm... Maybe if you decipher it you will be able to get the key? Here are some hints to get you started: can you tell how this is coded? Once you break this then you may need to consult some ancient rulers for help to find your hint. 

9. Base64 (UTF-8) message: 
``` 
VmggVmggcXhmIGNxbiBSWSBjamt1bmIgY2Rhdy4uLiBIeGQgamFuIGp1dnhiYyBjcW5hbi4gQ3FuIGJ4dWRjcnh3IHJiIHFybW1udyBydyBjcW4gYmp1amFoIHJ3b3hhdmpjcnh3IGJ4dm5mcW5hbiByd2VyYnJrdW4uIExqdyBoeGQgb3J3bSByYyB4dyB1cnduIHdyd253cnduPw==
```
10. *Recognize that this is base64 and use a base64 decoder to get... [Cesear Cipher (+9)] 

```
Vh Vh qxf cqn RY cjkunb cdaw... Hxd jan juvxbc cqnan. Cqn bxudcrxw rb qrmmnw rw cqn bjujah rwoxavjcrxw bxvnfqnan rwerbrkun. Ljw hxd orwm rc xw urwn wrwnwrwn? 
```

11. *Decode Cesear Cipher (shift cipher) gives you: 

12. ``` My My how the IP tables turn... You are almost there. The solution is hidden in the salary information somewhere invisible. Can you find it on line ninenine? ```

13. Open the included excel file and look on line 99 for hidden text. The cell BH99 contains text written in white color. This is the flag and hint that leads to the next flag. 



## Flag Notes:  

